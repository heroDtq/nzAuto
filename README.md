# 自动化测试框架

#### 项目介绍
一个自动化测试中文化框架

#### 软件架构
软件架构采用:Selenium-Java


#### 安装教程

1. 下载Selenium-Java,Selenium-Server 两个包
2. 下载eclipse plugin with Hocon(mix josn,ini...outher config format File) 
 https://github.com/dragos/typesafe-config-eclipse
3. 下载Config包:https://mvnrepository.com/artifact/com.typesafe/config


#### 使用说明

1. eclipse中安装上述 Hocon编辑器插件
2. 项目中创建java项目,引用Selenium-Java,Selenium-Server,config三个包

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 语法规则
1. 数据结构遵循程序设计结构:顺序,选择,循环

	```
	[打开]{testUrl}
	
	[循环]3
		[打开]{testUrl}
	
	[判断]3=3,对
		[打开]{testUrl}
	
	[变量]value=3	
	[选择]
		1,([打开]{testUrl})
		2,([打开]{testUrl})
		E,[退出]
		
	```

2. 变量设计遵循单行设计: 

	```
	[变量] A=3; // 变量值通常为字符串或者数字类型
	```

3. 语句
	
	```
	一元表达运算:+,-,*,/
	
	A={A}+3
	二元表达运算: && 或 || 和,!非, !=不等于,==相等;
	
	```
