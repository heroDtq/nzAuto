package com.nz.auto.core;

import com.nz.auto.framework.Log;

public class Complex {

	private String DialectOrderName;
	private String OrderName;
	private String ClassMappingName;
	private int ArgsNum;
	private int StrucLevel;
	/**
	 * @return the dialectOrderName
	 */
	public String getDialectOrderName() {
		return DialectOrderName;
	}
	/**
	 * @return the argsNum
	 */
	public int getArgsNum() {
		return ArgsNum;
	}
	/**
	 * @param argsNum the argsNum to set
	 */
	public void setArgsNum(int argsNum) {
		ArgsNum = argsNum;
	}
	/**
	 * @return the strucLevel
	 */
	public int getStrucLevel() {
		return StrucLevel;
	}
	/**
	 * @param strucLevel the strucLevel to set
	 */
	public void setStrucLevel(int strucLevel) {
		StrucLevel = strucLevel;
	}
	/**
	 * @param dialectOrderName the dialectOrderName to set
	 */
	public void setDialectOrderName(String dialectOrderName) {
		DialectOrderName = dialectOrderName;
	}
	/**
	 * @return the orderName
	 */
	public String getOrderName() {
		return OrderName;
	}
	/**
	 * @param orderName the orderName to set
	 */
	public void setOrderName(String orderName) {
		OrderName = orderName;
	}
	/**
	 * @return the classMappingName
	 */
	public String getClassMappingName() {
		return ClassMappingName;
	}
	/**
	 * @param classMappingName the classMappingName to set
	 */
	public void setClassMappingName(String classMappingName) {
		ClassMappingName = classMappingName;
	}
	//测试方法
	public static void main(String[] args) {
		Complex complex=new Complex();
		complex.setDialectOrderName("切换新窗口");
		complex.setOrderName("switchNewWindow");
		complex.setArgsNum(1);
		complex.setStrucLevel(2);
		complex.setClassMappingName("com.nz.auto.instructions.window.switchNewWindow");
		Log.debug(complex);

	}

}
