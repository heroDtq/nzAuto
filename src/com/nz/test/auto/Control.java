package com.nz.test.auto;



import com.nz.auto.framework.Log;
import com.nz.test.auto.core.DriverPack;
import com.nz.test.auto.inter.CallBack;
import com.nz.test.auto.inter.OrderContent;
import com.nz.test.auto.inter.Runner;

public class Control implements CallBack{
	private OrderContent content;
	private DriverPack dr;
	private Runner runner;
	public Control(Runner runner,OrderContent oContent,DriverPack dr) {
		this.content=oContent;
		this.dr=dr;
		this.runner=runner;
	}
	public void dealCase() {		
		runner.processDetail(this);
	}
	@Override
	public void returnResult(String result) {
		Log.info("返回的结果:{}",result);
	}
	@Override
	public OrderContent getContent() {
		return this.content;
	}
	@Override
	public DriverPack getDriver() {
		return this.dr;
	}

	
}
