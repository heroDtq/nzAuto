package com.nz.test.auto;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.nz.test.auto.core.CaseFileList;
import com.nz.test.auto.core.DriverPack;
import com.nz.test.auto.core.ReadCase;
import com.nz.test.auto.core.Robot;
import com.nz.test.auto.inter.FuncContent;


public class RunTest {

	public void run(int times) {
		runOrder();
	}

	public void runOrder() {
		List<String[]> list = CaseFileList.getCaseList();
		ReadCase readCase = new ReadCase();
		for (String[] item : list) {
			DriverPack driverInit = new DriverPack();// 每一个新的用例就重启一次浏览器
			Map<String, FuncContent> preTo = readCase.ContentConvert(item[0], item[1]);
			Iterator<Entry<String, FuncContent>> iterator=preTo.entrySet().iterator();
			while(iterator.hasNext()) {
				Entry<String, FuncContent> entry=iterator.next();
				Robot robot=new Robot();
				robot.setRobotHead(driverInit);
				robot.setRobotBody(entry.getKey(), entry.getValue().getArguments(), entry.getValue().getPreToCaluse());
				robot.Start();				
			}
			driverInit.quit();
		}
	}

	
	

	/**
	 * 测试方法
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		RunTest test = new RunTest();
		test.run(1);
	}
}
