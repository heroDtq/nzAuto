package com.nz.test.auto.core;

import java.util.List;

import com.nz.test.auto.inter.OrderContent;

public class InstructionStruc {
	/**
	 * @return the content
	 */
	public OrderContent getContent() {
		return content;
	}
	/**
	 * @param orderContent the content to set
	 */
	public void setContent(OrderContent orderContent) {
		this.content = orderContent;
	}
	/**
	 * @return the driverPack
	 */
	public DriverPack getDriverPack() {
		return driverPack;
	}
	/**
	 * @param driverPack the driverPack to set
	 */
	public void setDriverPack(DriverPack driverPack) {
		this.driverPack = driverPack;
	}
	private OrderContent content;
	private DriverPack driverPack;
}
