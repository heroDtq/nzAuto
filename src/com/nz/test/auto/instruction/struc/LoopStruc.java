/**
 * 
 */
package com.nz.test.auto.instruction.struc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.nz.test.auto.core.DialectMap;
import com.nz.test.auto.core.DriverPack;
import com.nz.test.auto.core.InstructionStruc;
import com.nz.test.auto.core.Log;
import com.nz.test.auto.core.MultiTools;
import com.nz.test.auto.core.Robot;
import com.nz.test.auto.core.WordStrcImp;
import com.nz.test.auto.inter.CallBack;
import com.nz.test.auto.inter.FuncContent;
import com.nz.test.auto.inter.OrderContent;
import com.nz.test.auto.inter.Runner;

/**
 * 循环结构
 * @author NXQ
 *
 */
public class LoopStruc  extends InstructionStruc implements Runner {


	@Override
	public void processDetail(CallBack callBack) {
		try {
			this.setContent(callBack.getContent());
			this.setDriverPack(callBack.getDriver());			
			Loops(getContent(),getDriverPack());
			Thread.sleep(1000);
		} catch (Exception e) {
		}
		callBack.returnResult("循环url");
	}
	
	public  void Loops(OrderContent oContent,DriverPack driverInit) {
		String loopTimes=oContent.getArguments().get(0);
		List<String>   content=oContent.getFunContent();
		for(int i=0;i<Integer.parseInt(loopTimes);i++) {
			Log.info("循环第{}次",i+1);
			List<FuncContent>listFunc=convertContent(content);
			Iterator<FuncContent> iterator=listFunc.iterator();
			while(iterator.hasNext()) {
				FuncContent entry=iterator.next();
				Robot robot=new Robot();
				robot.setRobotHead(driverInit);
				robot.setRobotBody(entry.getClassName(), entry.getArguments(), entry.getPreToCaluse());
				robot.Start();
				iterator.remove();
			}
		}
	}
	
	
	

	
	public List<FuncContent> convertContent(List<String> preToContent) {
		MultiTools multiTools=new MultiTools();
		List<String> preList=preToContent;
		List<FuncContent> listFunc=new ArrayList<>();
		WordStrcImp wordStrcImp=new WordStrcImp();//词性分析器
		DialectMap dialectMap=new DialectMap();
		for(int i=0;i<preList.size();i++) {
			String content=preList.get(i);
			String or=wordStrcImp.getOrder(content);//方言指令
			String classNameKey=dialectMap.getClassName(or);
			Integer num=dialectMap.getParam(or);
			content=content.replaceAll("\\["+or+"\\]", "");
			List<String> contentList=Arrays.asList(content.split(",",num+1));
			List<String>  cList=new ArrayList<String>(contentList);
			FuncContent funcContent=new FuncContent();
			
			List<String> arguments=new ArrayList<String>();
			List<String> preToCaluse=new ArrayList<String>();
			for(int j=0;j<num;j++) {
				arguments.add(((String) cList.get(0)).replaceAll("\\(|\\)", ""));
				cList.remove(cList.get(0));//去除指令后的语句,列表化后个数多于参数个数,剩下数据列入子句
			}
			List<String> detailList=null;
			if(cList.size()>0) {
				//TODO: 判断括号成对匹配,嵌套 引入工具类MultiTools待完善
				detailList=multiTools.digMatchSubString(cList.get(0), 1);
				for(int j=0;j<detailList.size();j++) {
					detailList.set(j, detailList.get(j));
				}
			}		
			
			preToCaluse=detailList;
			funcContent.setClassName(classNameKey);
			funcContent.setArguments(arguments);
			funcContent.setPreToCaluse(preToCaluse);
			listFunc.add(funcContent);			
		}
		return listFunc;
	
	}
}
