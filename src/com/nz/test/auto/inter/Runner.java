/**
 * 
 */
package com.nz.test.auto.inter;



/**
 * 执行接口
 * @author NXQ
 *
 */
public interface Runner {

	public void processDetail(CallBack callBack);
}
